﻿CREATE TABLE [nilnul].[Acc]
(
	[Id] bigINT NOT NULL PRIMARY KEY identity
	,
	[name] varchar(400)
	,
	[pass_tip] nvarchar(400)
	,
	_time datetime default getUtcDate()
	,
	_memo nvarchar(max)
);
