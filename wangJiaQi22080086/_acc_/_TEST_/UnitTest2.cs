﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace nilnul._acc_._TEST_
{
    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void TestMethod1()
        {
            var p = new nilnul.acc_.Pass("123456");

            var check = new nilnul.acc_.pass.LogOn(
                (string pass) => pass.Contains("123456")?true:false
                );

            Assert.IsTrue(check(p.val));
            Assert.IsTrue(check("abc12345678"));
            Assert.IsFalse(check("abc123"));

        }
    }
}
