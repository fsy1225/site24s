﻿CREATE TABLE [nilnul].[Acc]
(
	[id] bigINT NOT NULL PRIMARY KEY identity
	,
	[name] nvarchar(4000) 
	,
	[pass_salt] nvarchar(4000)
	,
	[pass] nvarchar(4000) --must be hashed
)
