﻿CREATE proc [nilnul.acc.token].[Logon]
(
	@name nvarchar(50),
	@pass nvarchar(50),
	@expire datetime null out
	,
	@token uniqueidentifier =null out
	,
	@acc bigint = null out
)
AS
BEGIN
set nocount on
--SET FMTONLY OFF

set @acc =(
	select top 1 id from [nilnul].Acc
		where UPPER( name)=upper(@name) and [pass]=@pass
);

if @acc is null begin
	return 1;
end
declare @tokens table([token] uniqueidentifier);
if @expire is null begin
	insert [nilnul.acc].[Token](
		[acc] 
	)
		output inserted.[token] into @tokens
		values(
			@acc
		)
	;
end
else begin
	insert [nilnul.acc].[Token](
		[acc], expire 
	)
		output inserted.[token] into @tokens
		values(
			@acc,
		
			@expire
		)
	;
end

set @token= (select top 1 [token] from @tokens);

END
