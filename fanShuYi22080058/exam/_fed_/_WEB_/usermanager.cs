﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class UserManager
{
    private static List<User> users = new List<User>();

    public static bool RegisterUser(string username, string password)
    {
        if (users.Any(u => u.Username == username))
        {
            return false;
        }

        users.Add(new User { Username = username, Password = password });
        return true;
    }

    public static bool LoginUser(string username, string password)
    {
        return users.Any(u => u.Username == username && u.Password == password);
    }
}