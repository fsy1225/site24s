﻿using System;
using System.Web.UI;

namespace WebApplication1
{
    public partial class Register : Page
    {
        protected void btnRegister_Click(object sender, EventArgs e)
        {
            string username = txtUsername.Text;
            string password = txtPassword.Text;

            if (UserManager.RegisterUser(username, password))
            {
                Response.Write("Registration successful");
            }
            else
            {
                Response.Write("Username already exists");
            }
        }
    }
}