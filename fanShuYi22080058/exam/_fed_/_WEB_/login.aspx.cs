﻿using System;
using System.Web.UI;

namespace WebApplication1
{
    public partial class Login : Page
    {
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string username = txtUsername.Text;
            string password = txtPassword.Text;

            if (UserManager.LoginUser(username, password))
            {
                Response.Write("Login successful");
            }
            else
            {
                Response.Write("Login failed");
            }
        }
    }
}