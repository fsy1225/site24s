﻿delete [nilnul].[Acc]
	output deleted.* --optional
	where id = (select max(id) from [nilnul].[Acc])   --optional; but you should add this line to avoid delete all            