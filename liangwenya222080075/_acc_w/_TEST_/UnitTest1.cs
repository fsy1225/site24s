﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using nilnul._acc_._LINQ_;
using System;

namespace nilnul.acc_._TEST_
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            using (var db= new nilnul._acc_._LINQ_.DataClasses1DataContext())
            {
                db.Acc.InsertOnSubmit(
                    new Acc()
                    {
                        name = "lisi"
                        ,
                        pass_tip = "the name of my primary school"
                    }
                    );
                db.SubmitChanges();
            }
        }
    }
}
