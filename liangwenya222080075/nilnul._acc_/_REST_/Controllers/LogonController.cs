﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace nilnul._acc_._WEB_.Controllers
{
    /// <summary>
    /// model
    /// </summary>
    /// 
    public class Data
    {
        public string name { get; set; }
        public string pass { get; set; }
    }


    public class LogonController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public Guid? Post([FromBody] Data value)
        {
            return nilnul.acc.token.Logon.logon(value.name, value.pass);

        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}