﻿< !DOCTYPE html >
    <html>
        <head>
            <title>Music Player</title>
        </head>
        <body>
            <audio id="music" controls>
                <source src="music.mp3" type="audio/mpeg">
                    Your browser does not support the audio element.
    </audio>

                <button onclick="playMusic()">Play</button>
                <button onclick="pauseMusic()">Pause</button>

                <script>
                    var music = document.getElementById("music");

                    function playMusic() {
                        music.play();
        }

                    function pauseMusic() {
                        music.pause();
        }
                </script>
</body>
</html>