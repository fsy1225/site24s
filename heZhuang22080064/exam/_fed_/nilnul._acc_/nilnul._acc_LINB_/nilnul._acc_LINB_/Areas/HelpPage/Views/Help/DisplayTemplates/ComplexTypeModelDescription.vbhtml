@Imports nilnul._acc_LINB_.Areas.HelpPage.ModelDescriptions
@ModelType ComplexTypeModelDescription
@Html.DisplayFor(Function(m) Model.Properties, "Parameters")
