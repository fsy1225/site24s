﻿@Imports System.Web.Mvc
@ModelType HandleErrorInfo
@Code
    ViewBag.Title = "已锁定"
End Code

<hgroup>
    <h1 class="text-danger">已锁定。</h1>
    <h2 class="text-danger">此帐户已锁定，请稍后重试。</h2>
</hgroup>
