﻿< !DOCTYPE html >
    <html>
        <head>
            <title>Music Player</title>
        </head>
        <body>
            <audio id="music" controls>
                <source src="music1.mp3" type="audio/mpeg">
    </audio>

                <button onclick="changeMusic()">Change Music</button>

                <script>
                    var music = document.getElementById("music");
                    var currentMusic = 1;

                    function changeMusic() {
            if (currentMusic === 1) {
                        music.src = "music2.mp3";
                    currentMusic = 2;
            } else {
                        music.src = "music1.mp3";
                    currentMusic = 1;
            }
                    music.play();
        }
                </script>
</body>
</html>