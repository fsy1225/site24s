﻿< !DOCTYPE html >
    <html>
        <head>
            <title>Volume Adjuster</title>
        </head>
        <body>
            <h1>Volume Adjuster</h1>
            <input type="range" id="volumeRange" min="0" max="100" value="50" step="1">
                <span id="volumeValue">50</span>

                <script>
                    const volumeRange = document.getElementById('volumeRange');
                    const volumeValue = document.getElementById('volumeValue');

                    volumeRange.addEventListener('input', function() {
            const volume = volumeRange.value;
                    volumeValue.textContent = volume;
                    // You can use the volume value to adjust the volume of an audio element or any other element that requires volume adjustment
                    console.log('Volume adjusted to: ' + volume);
        });
                </script>
</body>
</html>