﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._acc
{
    /// <summary>
    /// list item of different types of account
    /// </summary>
    enum Li
    {
        /// <summary>
        /// only a password,no username
        /// eg:
        ///     elock,phone screen unlock,token
        /// </summary>
        password,
        ///<summary>
        ///手机号
        ///</summary>
        Phone,
        ///<summary>
        ///人脸识别
        ///</summary>
        Face,
        ///<summary>
        ///指纹识别
        /// </summary>
        FingerPrint,
        ///<summary>
        ///虹膜识别
        /// </summary>
        iris
    }
}
