﻿CREATE TABLE [nilnul.acc.usr].[Token]
(
	[id] bigINT NOT NULL PRIMARY KEY identity
	,
	acc bigint not null references [nilnul].Acc(id)
	,
	token nvarchar(400) not null default newid()
	,
	due datetime default dateadd(day,7,getUtcdate())
)

