﻿CREATE TABLE [nilnul].[Acc]
(
	[id] bigINT NOT NULL PRIMARY KEY identity
	,
	[name] nvarchar(400)
	,
	[bill type] nvarchar(400)
	,
	_time datetime default getUtcDate()
	,
	[amount] nvarchar(max), 
    [remark] NCHAR(10) NULL 
);
