﻿insert [nilnul].Acc(
	name, pass_tip
)
	usr Application

DROP TABLE IF EXISTS UserData;	/*用户信息*/
DROP TABLE IF EXISTs Account;	/*用户账本数据*/

CREATE TABLE UserData
(
ID NCHAR(10) PRIMARY KEY,	/*用户ID*/
UserPassword NCHAR(32),			/*用户密码*/
UserMail NCHAR(30),			/*用户邮箱*/
UserPhoto image,				/*用户照片*/
UserName NCHAR(30),			/*用户名*/
);

CREATE TABLE Account
(
UserID NCHAR(10),
InOrOut NCHAR(10),
ConsumeType NCHAR(10),
Price FLOAT,
NOTE NCHAR(200),
RecordDate DATE,
);

/*管理员初始账号*/
INSERT INTO UserData(ID,UserPassword,UserMail,UserName) VALUES('100001','   ','wenwen@txw.com','小骆txw');
GO


