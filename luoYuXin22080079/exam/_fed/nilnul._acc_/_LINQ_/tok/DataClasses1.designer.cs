﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。
//     运行时版本:4.0.30319.42000
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
// </auto-generated>
//------------------------------------------------------------------------------

namespace nilnul._acc_._LINQ_.tok
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="my")]
	public partial class DataClasses1DataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region 可扩展性方法定义
    partial void OnCreated();
    partial void InsertToken(Token instance);
    partial void UpdateToken(Token instance);
    partial void DeleteToken(Token instance);
    #endregion
		
		public DataClasses1DataContext() : 
				base(global::nilnul._acc_._LINQ_.Properties.Settings.Default.myConnectionString1, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<Token> Token
		{
			get
			{
				return this.GetTable<Token>();
			}
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="[nilnul._acc.usr.tok].Logon")]
		public int Logon([global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NVarChar(50)")] string name, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NVarChar(50)")] string pass, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="DateTime")] ref System.Nullable<System.DateTime> due, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="UniqueIdentifier")] ref System.Nullable<System.Guid> token, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="BigInt")] ref System.Nullable<long> acc)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), name, pass, due, token, acc);
			due = ((System.Nullable<System.DateTime>)(result.GetParameterValue(2)));
			token = ((System.Nullable<System.Guid>)(result.GetParameterValue(3)));
			acc = ((System.Nullable<long>)(result.GetParameterValue(4)));
			return ((int)(result.ReturnValue));
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="[nilnul._acc.usr].Token")]
	public partial class Token : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private long _id;
		
		private long _acc;
		
		private string _token1;
		
		private System.Nullable<System.DateTime> _due;
		
    #region 可扩展性方法定义
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnidChanging(long value);
    partial void OnidChanged();
    partial void OnaccChanging(long value);
    partial void OnaccChanged();
    partial void Ontoken1Changing(string value);
    partial void Ontoken1Changed();
    partial void OndueChanging(System.Nullable<System.DateTime> value);
    partial void OndueChanged();
    #endregion
		
		public Token()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", AutoSync=AutoSync.OnInsert, DbType="BigInt NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public long id
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((this._id != value))
				{
					this.OnidChanging(value);
					this.SendPropertyChanging();
					this._id = value;
					this.SendPropertyChanged("id");
					this.OnidChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_acc", DbType="BigInt NOT NULL")]
		public long acc
		{
			get
			{
				return this._acc;
			}
			set
			{
				if ((this._acc != value))
				{
					this.OnaccChanging(value);
					this.SendPropertyChanging();
					this._acc = value;
					this.SendPropertyChanged("acc");
					this.OnaccChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="token", Storage="_token1", DbType="NVarChar(400) NOT NULL", CanBeNull=false)]
		public string token1
		{
			get
			{
				return this._token1;
			}
			set
			{
				if ((this._token1 != value))
				{
					this.Ontoken1Changing(value);
					this.SendPropertyChanging();
					this._token1 = value;
					this.SendPropertyChanged("token1");
					this.Ontoken1Changed();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_due", DbType="DateTime")]
		public System.Nullable<System.DateTime> due
		{
			get
			{
				return this._due;
			}
			set
			{
				if ((this._due != value))
				{
					this.OndueChanging(value);
					this.SendPropertyChanging();
					this._due = value;
					this.SendPropertyChanged("due");
					this.OndueChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
#pragma warning restore 1591
