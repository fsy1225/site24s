﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.acc.token
{
	public class Logon
	{
        public partial class Login : Form
        {
            public string code;         //储存生成验证码
            public Login()
            {
                InitializeComponent();
                init();
            }

            #region 初始化参数
            private void init()
            {
                this.MaximizeBox = false;       //禁止最大化
                                                //使标签透明化
                title_lab.BackColor = Color.Transparent;
                account_lab.BackColor = Color.Transparent;
                password_lab.BackColor = Color.Transparent;
                vericode_lab.BackColor = Color.Transparent;
                veri_lab.BackColor = Color.Transparent;

                //使按钮透明化
                login_btn.FlatStyle = FlatStyle.Flat;
                login_btn.ForeColor = Color.Transparent;
                login_btn.BackColor = Color.Transparent;
                login_btn.FlatAppearance.BorderSize = 1;
                login_btn.FlatAppearance.MouseOverBackColor = Color.Transparent;
                login_btn.FlatAppearance.MouseDownBackColor = Color.Transparent;

                register_btn.FlatStyle = FlatStyle.Flat;
                register_btn.ForeColor = Color.Transparent;
                register_btn.BackColor = Color.Transparent;
                register_btn.FlatAppearance.BorderSize = 1;
                register_btn.FlatAppearance.MouseOverBackColor = Color.Transparent;
                register_btn.FlatAppearance.MouseDownBackColor = Color.Transparent;

                forget_linkbtn.FlatStyle = FlatStyle.Flat;
                forget_linkbtn.ForeColor = Color.Transparent;
                forget_linkbtn.BackColor = Color.Transparent;

                checkBox1.BackColor = Color.Transparent;

                //验证码生成
                CreatVeriCode();
            }
            #endregion

            #region 生成随机验证码
            private void CreatVeriCode()
            {
                //随机实例化 
                code = "";
                Random ran = new Random();
                int number;
                char code1;
                //取五个数 
                for (int i = 0; i < 5; i++)
                {
                    number = ran.Next();
                    if (number % 2 == 0)
                        code1 = (char)('0' + (char)(number % 10));
                    else
                        code1 = (char)('A' + (char)(number % 26)); //转化为字符 

                    this.code += code1.ToString();
                }

                vericode_lab.Text = code;

            }
            #endregion

            #region 登录按钮事件（连接数据库验证用户名和密码）
            private void login_btn_Click(object sender, EventArgs e)
            {

                string username = account_txt.Text.Trim();
                string password = EncryptWithMD5(password_txt.Text.Trim());
                //创建数据库连接对象
                string sqlconf = "Data Source = .;Initial Catalog = Application;Integrated Security = SSPI";
                SqlConnection sqlConnection = new SqlConnection(sqlconf);
                sqlConnection.Open();

                string sql = "select ID,UserPassword from UserData where UserName = '" + username + "'and UserPassword = '" + password + "'";
                SqlCommand cmd = new SqlCommand(sql, sqlConnection);
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows && vericode_txt.Text.ToLower() == code.ToLower())
                {
                    new Main(username).Show();

                    this.Hide();
                }
                else if (reader.HasRows && vericode_txt.Text.ToLower() != code.ToLower())
                {
                    MessageBox.Show("验证码有误，登录失败！", "警告");
                    CreatVeriCode();
                    return;
                }
                else
                {
                    MessageBox.Show("账号密码有误，登录失败！", "警告");
                    return;
                }
                reader.Close();
                sqlConnection.Close();
            }
            #endregion

            #region 注册界面
            private void register_btn_Click(object sender, EventArgs e)
            {
                new Register().Show();
            }
            #endregion

            #region MD5加密算法
            private string EncryptWithMD5(string source)//MD5加密
            {
                byte[] sor = Encoding.UTF8.GetBytes(source);
                MD5 md5 = MD5.Create();
                byte[] result = md5.ComputeHash(sor);
                StringBuilder strbul = new StringBuilder(40);
                for (int i = 0; i < result.Length; i++)
                {
                    strbul.Append(result[i].ToString("x2"));//加密结果"x2"结果为32位,"x3"结果为48位,"x4"结果为64位
                }
                return strbul.ToString();
            }
            #endregion

            #region 点击验证码标签刷新验证码
            private void vericode_lab_Click(object sender, EventArgs e)
            {
                CreatVeriCode();
            }
            #endregion

            private void forget_linkbtn_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
            {
                new Forget().Show();
            }

            private void checkBox1_CheckedChanged(object sender, EventArgs e)
            {
                if (checkBox1.Checked)
                {
                    //复选框被勾选，显示密码
                    password_txt.PasswordChar = new char();
                }
                else
                {
                    //复选框为被勾选，隐藏密码
                    password_txt.PasswordChar = '*';
                }
            }
        }
    }
