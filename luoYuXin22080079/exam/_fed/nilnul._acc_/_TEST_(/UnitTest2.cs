﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace nilnul._acc_._TEST_
{
    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void TestMethod1()
        {
            var p = new nilnul.acc_.Pass("100001");

            var check = new nilnul.acc_.pass.LogOn(
                (string pass) => pass.Contains("wenwen@txw.com")?true:false
             );

             Assert.IsTrue( check(p.val) );
             Assert.IsTrue( check("100001") );
             Assert.IsFalse( check("123456") );



        }
    }
}
