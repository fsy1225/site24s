﻿namespace WindowsFormsApplication1
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.注册账号 = new System.Windows.Forms.TextBox();
            this.注册密码 = new System.Windows.Forms.TextBox();
            this.确认密码 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.确认注册 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.重置 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // 注册账号
            // 
            this.注册账号.Font = new System.Drawing.Font("宋体", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.注册账号.ForeColor = System.Drawing.SystemColors.WindowText;
            this.注册账号.Location = new System.Drawing.Point(219, 110);
            this.注册账号.Name = "注册账号";
            this.注册账号.Size = new System.Drawing.Size(156, 39);
            this.注册账号.TabIndex = 3;
            // 
            // 注册密码
            // 
            this.注册密码.Font = new System.Drawing.Font("宋体", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.注册密码.ForeColor = System.Drawing.SystemColors.WindowText;
            this.注册密码.Location = new System.Drawing.Point(219, 196);
            this.注册密码.Name = "注册密码";
            this.注册密码.PasswordChar = '*';
            this.注册密码.Size = new System.Drawing.Size(156, 39);
            this.注册密码.TabIndex = 4;
            // 
            // 确认密码
            // 
            this.确认密码.Font = new System.Drawing.Font("宋体", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.确认密码.ForeColor = System.Drawing.SystemColors.WindowText;
            this.确认密码.Location = new System.Drawing.Point(219, 276);
            this.确认密码.Name = "确认密码";
            this.确认密码.PasswordChar = '*';
            this.确认密码.Size = new System.Drawing.Size(156, 39);
            this.确认密码.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(82, 116);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 28);
            this.label1.TabIndex = 6;
            this.label1.Text = "账号";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(82, 194);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 28);
            this.label2.TabIndex = 7;
            this.label2.Text = "密码";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(52, 282);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 28);
            this.label3.TabIndex = 8;
            this.label3.Text = "确认密码";
            // 
            // 确认注册
            // 
            this.确认注册.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.确认注册.Location = new System.Drawing.Point(106, 367);
            this.确认注册.Name = "确认注册";
            this.确认注册.Size = new System.Drawing.Size(102, 47);
            this.确认注册.TabIndex = 9;
            this.确认注册.Text = "确定";
            this.确认注册.UseVisualStyleBackColor = true;
            this.确认注册.Click += new System.EventHandler(this.确认注册_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(116, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(244, 28);
            this.label4.TabIndex = 10;
            this.label4.Text = "欢迎来到注册界面";
            // 
            // 重置
            // 
            this.重置.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.重置.Location = new System.Drawing.Point(273, 367);
            this.重置.Name = "重置";
            this.重置.Size = new System.Drawing.Size(102, 47);
            this.重置.TabIndex = 11;
            this.重置.Text = "重置";
            this.重置.UseVisualStyleBackColor = true;
            this.重置.Click += new System.EventHandler(this.重置_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(489, 527);
            this.Controls.Add(this.重置);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.确认注册);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.确认密码);
            this.Controls.Add(this.注册密码);
            this.Controls.Add(this.注册账号);
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "注册框";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox 注册账号;
        private System.Windows.Forms.TextBox 注册密码;
        private System.Windows.Forms.TextBox 确认密码;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button 确认注册;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button 重置;
    }
}