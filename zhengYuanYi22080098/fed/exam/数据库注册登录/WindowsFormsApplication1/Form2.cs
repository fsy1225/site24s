﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void 重置_Click(object sender, EventArgs e)
        {
            注册账号.Text = null;
            注册密码.Text = null;
            确认密码.Text = null;
        }

        private void 确认注册_Click(object sender, EventArgs e)
        {
            Regex panduan = new Regex(@"\d\d\d\d\d");
            bool panduan1 = panduan.IsMatch(注册账号.Text);
            bool panduan2 = panduan.IsMatch(注册密码.Text);
            bool panduan3 = panduan.IsMatch(确认密码.Text);
            if (panduan1 == false)
                MessageBox.Show("请输入5位数字的账号");
            else
            {
                if (panduan2 == false)
                    MessageBox.Show("请输入5位数字的密码");
                else
                {
                    if(panduan3==false)
                    MessageBox.Show("确认密码和密码不一致");
                }
            }
            if(panduan1 && panduan2 && panduan3)
            {
                if (注册密码.Text == 确认密码.Text)
                {
                    string connStr = "server=localhost;user=root;database=test;port=3306;password=weixiao";
                    MySqlConnection conn = new MySqlConnection(connStr);
                    try
                    {
                        conn.Open();
                    }
                    catch (MySqlException ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    string sqlstr = "INSERT INTO `users` (`account`,`password`) VALUES(" + 注册账号.Text + "," + 确认密码.Text + ");";
                    MySqlCommand cmd = new MySqlCommand(sqlstr, conn);
                    int result = cmd.ExecuteNonQuery();
                    if (result == 1)
                        MessageBox.Show("注册成功");
                    else
                        MessageBox.Show("注册失败");
                    conn.Close();
                }
                else
                    MessageBox.Show("确认密码和密码不一致");
            }
        }
    }
}
