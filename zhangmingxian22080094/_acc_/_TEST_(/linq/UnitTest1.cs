﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace nilnul._acc_._TEST_.linq
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            using (var db= new nilnul._acc_._LINQ_.DataClasses1DataContext())
            {
                db.Acc.InsertOnSubmit(
                    new _LINQ_.Acc() { 
                        name="lisi"
                        ,
                        pass_tip="the name of my primary school"
                        ,
                    }    
                );
                db.SubmitChanges();
            }
        }
    }
}
