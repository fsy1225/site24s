﻿CREATE TABLE [nilnul].[Acc]
(
	[id] bigINT NOT NULL PRIMARY KEY identity
	,
	[name] nvarchar(400)
	,
	[pass_tip] nvarchar(400)
	,
	_time datetime default getUtcDate()
	,
	_memo nvarchar(max) 
);
