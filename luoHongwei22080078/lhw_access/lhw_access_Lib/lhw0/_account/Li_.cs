﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lhw._account
{
    /// <summary>
    /// 账号不同类型的存储项目
    /// </summary>
    enum Li_
    {
        /// <summary>
        /// 密码登录,不使用账号
        /// 例如:手机号,token,锁屏密码
        /// </summary>
        Password,
        ///<summary>
        /// 手机号登录
        /// </summary>
        Phone,
        ///<summary>
        ///人脸登录
        /// </summary>
        Face,
        ///<summary>
        /// 指纹登录
        /// </summary>
        FingerPrint,
        ///<summary>
        /// 邮箱登录
        /// </summary>
        Email,
    }
}
