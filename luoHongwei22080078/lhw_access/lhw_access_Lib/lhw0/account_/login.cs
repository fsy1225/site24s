﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lhw.account_
{
    /// <summary>
    /// 用户名与密码
    /// </summary>
    class login : Account1
    {
        private string _username;          // username 的存储域
        public string username => _username;  // 将内部username返还给调用者

        private string _password;
        public string password => _password;
    }
}
