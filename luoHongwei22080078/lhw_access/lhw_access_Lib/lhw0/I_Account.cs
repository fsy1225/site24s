﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lhw
{
    /// <summary>
    /// 标记接口
    /// </summary>
    interface I_Account
    {
    }
    /// <summary>
    /// 成员接口
    /// </summary>
    interface Account1: I_Account  // 冒号是继承I_Account
    {
        /// <summary>
        /// 用户名
        /// </summary>
        string username { get;  }
    }
}
