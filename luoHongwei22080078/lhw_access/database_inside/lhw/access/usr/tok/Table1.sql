﻿CREATE TABLE [lhw.access.usr].[Token]
(
	[id] bigINT NOT NULL PRIMARY KEY identity
	,
	acc bigint not null references [lhw].access(id)
	,
	token nvarchar(400) not null default newid()
	,
	expire datetime default dateadd(day,7,getUtcdate())
)

