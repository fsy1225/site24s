﻿CREATE TABLE [lhw].[access]
(
	[Id] bigINT NOT NULL PRIMARY KEY identity,
	[name] nvarchar(400),
	[password_tips] nvarchar(400),
	_time datetime default getUtcDate(),
	_memo nvarchar(max)
);
