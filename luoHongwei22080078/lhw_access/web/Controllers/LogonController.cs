﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace lhw_access_web.Controllers
{
    public class LogonController : ApiController
    {
        public class Data
        {
            public string name { get; set; }
            public string pass { get; set; }
        }

        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public Guid? Post([FromBody] Data value)
        {
            return lhw.access.token.Logon.logon(value.name, value.pass);
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}