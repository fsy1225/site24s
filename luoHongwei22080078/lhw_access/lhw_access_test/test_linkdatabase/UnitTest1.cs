﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace lhw_test_account.test_linkdatabase
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            // using:表示在使用结束后,自动释放资源(内存)
            using (var db = new lhw_acc_link_database.DataClasses1DataContext())
            {
                db.access.InsertOnSubmit(
                    new lhw_acc_link_database.access()
                    {
                        name = "wxk",
                        password_tips = "the time of primary school",
                        _memo = "test",
                    }
                ) ;
                db.SubmitChanges();

            }
        }
    }
}
