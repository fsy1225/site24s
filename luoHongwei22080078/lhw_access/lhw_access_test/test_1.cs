﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace lhw_account_test
{
    [TestClass]  // 属性
    public class test_1
    {
        [TestMethod]
        public void TestMethod1()
        {
            // lhw.account_是Password.cs的命名空间,下述密码的确认前的lhw.account_password也是Logon.cs命名空间
            var p = new lhw.account_.Password("123456");  // 设置密码: 123456,将密码串给lhw_account_Lib的Password.cs的Password方法,存入密码

            var check = new lhw.account_.password.Logon(
                (string password) => password.Contains("123456") ? true : false  // 定义password,将123456传给password,调用lhw_account_Lib中的Logon.cs方法确定密码是否正确
                );

            Assert.IsTrue(check(p.value));  // Assert.IsTrue()断言，如果测试的密码与设置密码一样则通过,不一样则报错:测试不通过

            Assert.IsTrue(check("ab123456"));

            //Assert.IsTrue(check("abc123456"));
        }
    }
}
