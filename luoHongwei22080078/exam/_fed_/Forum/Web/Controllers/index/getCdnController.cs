﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Web.Http;

namespace Web.Controllers.index
{
    public class getCdnController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        [HttpGet]
        [Route("api/source/get_study/cdn")]
        public object Get(int id)
        {
            Debug.WriteLine("api/source/get_study/cdn 接口被调用");
            return Lib.index.getStudy.get_cdn_url();
        }

        [HttpPost]
        [Route("api/source/get_study/cdn")]
        // POST api/<controller>
        public object Post([FromBody] string value)
        {
            Debug.WriteLine("api/source/get_study/cdn 接口被调用");
            Debug.WriteLine(value);
            return Lib.index.getStudy.get_cdn_url();
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}