﻿using Newtonsoft.Json.Linq;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Web.Controllers.index
{
    public class getArticleController1 : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        [HttpGet]
        [Route("api/source/get_article")]
        public object Get(string id)
        {
            Debug.WriteLine("接口 api/source/get_article 收到请求");
            Debug.WriteLine(id);
            return Lib.index.getArticle.get_article();
        }

        // POST api/<controller>
        [HttpPost]
        [Route("api/source/get_article")]
        public object Post([FromBody] string value)
        {
            Debug.WriteLine("接口 api/source/get_article 收到请求");
            Debug.WriteLine(value);
            return Lib.index.getArticle.get_article();
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}