﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web.Http;

namespace WebApplication1.Controllers
{
    // DTO-接收数据
    public class Data
    {
        public string username { get; set; }
        public string password { get; set; }
    }
    public class loginController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        [Route("api/register")]
        // IHttpActionResult是创建异步 请求的
        // 登录操作
        public IHttpActionResult Register([FromBody] Data value)
        {
            return Ok();
        }

        // POST api/<controller>
        [HttpPost]
        [Route("api/login")]
        // IHttpActionResult是创建异步 请求的
        // 注册操作
        public Object Login([FromBody] Data value)
        {
            Debug.WriteLine("---控制器---\n执行登录方法");
            // 调用处理数据的方法
            return Lib.login.Login.login(value.username, value.password);

        }
        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}