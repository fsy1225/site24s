﻿using System;
using System.Diagnostics;

namespace Web.Lib.register
{
    public class Register
    {
        static public void register(string username, string password)
        {
            Debug.WriteLine("注册操作");
            using (var db = new LINQ.login.loginDataContext())
            {
                db.usr.InsertOnSubmit(
                    new LINQ.login.usr()
                    {
                        username = username,
                        password = password,
                        _time = DateTime.Now,
                        _memo = "注册..."
                    }
                    );
                db.SubmitChanges();
            }
        }
    }
}