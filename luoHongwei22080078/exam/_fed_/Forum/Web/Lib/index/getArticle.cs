﻿using LINQ.index;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace Web.Lib.index
{ 
    public class getArticle
    {
        public class sql_article_data
        {
            public int article_id;
            public string title;
            public string content;
        }
        public class Data
        {
            public string msg;
            public bool bol;
            public object data;
        }
        static public object get_article()
        {
            Debug.WriteLine("Lib的get_article()");
            // 数据库操作
            using (var db = new LINQ.index.getArticleDataContext())
            {
                Debug.WriteLine("请求帖子的数据库");
                // 查询
                var query = from r in db.article
                            select r;

                List<article> results = query.ToList();
                if (results.Count > 0)
                {
                    // 定义帖子列表
                    List<object> articles = new List<object>();
                    foreach (var r in results) {
                        var article = new sql_article_data
                        {
                            article_id = r.article_id,
                            title = r.title,
                            content = r.content,
                        };
                        articles.Add(article);

                    }
                    var reply = new Data
                    {
                        msg = "查询成功!",
                        bol = true,
                        data = articles,
                    };
                    return reply;
                    
                }
                else {
                    // 当表内无内容
                    var reply = new Data
                    {
                        msg = "哦豁,暂无内容,请联系管理员!",
                        bol = false,
                        data = { }
                    };
                    return reply;
                }
            }
        }
    }
}