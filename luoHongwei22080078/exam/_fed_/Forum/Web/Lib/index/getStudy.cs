﻿using LINQ.index;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Web.Lib.index
{
    public class getStudy
    {
        public class sql_cdn_data
        {
            public string name;
            public string imp_url;
            public string css_url;
        }
        public class Data
        {
            public string msg;
            public bool bol;
            public List<object> data;
        }
        static public object get_cdn_url()
        {
            Debug.WriteLine("Lib的get_cdn_url()");
            // 查询数据库的 coms.cdn 表
            using (var db = new LINQ.index.get_studyDataContext())
            {
                Debug.WriteLine("请求cdn链接的数据库");
                // 查询数据
                Debug.WriteLine("查询coms.cdn");
                var query = from r in db.cdn
                            select r;

                // 将查询结果转化为List
                List<cdn> results = query.ToList();
                Debug.WriteLine(results.Count);
                if (results.Count > 0)
                {
                    // 定义列表类型数据
                    List<object> cdns = new List<object>();
                    // 当表内有内容
                    foreach (var r in results)
                    {
                        var cdn = new sql_cdn_data
                        {
                            name = r.name,
                            imp_url = r.imp_url,
                            css_url = r.css_url,
                        };
                        cdns.Add(cdn);
                    };
                    var reply = new Data
                    {
                        msg = "查询成功!",
                        bol = true,
                        data = cdns

                    };
                    return reply;
                }
                else
                {
                    // 当表内无内容
                    var reply = new Data
                    {
                        msg = "哦豁,暂无内容,请联系管理员!",
                        bol = false,
                        data = { }
                    };
                    return reply;
                }
            }
        }
    }
}