﻿using LINQ.login;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.UI.WebControls;

using Web.Lib.register;


namespace WebApplication1.Lib.login
{
    public class Login
    {
        public class Data
        {
            public string msg;
            public bool bol;
        }
        static public Object login(string username, string password)
        {
            Debug.WriteLine("---Lib---\n登录操作");
            using (var db = new LINQ.login.loginDataContext())
            {
                // 查询表,判断用户是否注册
                Debug.WriteLine("查询...");
                var query = from r in db.usr
                            where r.username == username
                            select r;
                // 将查询结果转化为List
                List<usr> results = query.ToList();

                // Debug.WriteLine(string.Join(",", results.Select(i => i.ToString())));
                if (results.Count() > 0)
                {
                    Debug.WriteLine("已经注册");
                    foreach (var r in results)
                    {
                        if (password == r.password)
                        {
                            var msg = username + "登录成功!";
                            var reply = new Data { msg = msg, bol = true };
                            return reply;
                        }
                        else
                        {
                            var msg = username + ":密码错误!";
                            var reply = new Data { msg = msg, bol = false };
                            return reply;
                        }
                    };
                    return new Data { msg = "未知", bol = false };

                }
                else
                {
                    Debug.WriteLine("注册");
                    Register.register(username, password);
                    var msg = username + "注册成功!";
                    var reply = new Data { msg = msg, bol = true };
                    return reply;
                }
            }
        }

    }
}