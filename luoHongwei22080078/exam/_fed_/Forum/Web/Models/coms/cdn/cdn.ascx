﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="cdn.ascx.cs" Inherits="Web.Models.coms.cdn.cdn" %>



<link rel="stylesheet" href="https://unpkg.com/element-plus/lib/theme-chalk/index.css" runat="server" />  

<style>
    .explain{
        font-family:华文楷体;
        color:red;
        display:block;
        position:absolute;
        text-align:left;
    }

    .show_src{
        position:absolute;
        top:150px;
        font-family:华文楷体;
    }
</style>

<div class="explain">
    <p>
        在下述表格中<br />
        <b>Name</b>是第三方组件的名字<br />
        <b>Imp_url</b>是引入该组件的链接<br />
        <b>Css_url</b>是该组件的Css样式链接,
        有些第三方组件有,有些无
    </p>
</div>


<div class="show_src">
    <table border="1">
        <thead>
            <tr>
                <th>Name</th>
                <th>Imp_url</th>
                <th>Css_url</th>
            </tr>
        </thead>
        <tbody id="content">

        </tbody>
    </table>
</div>


<!-- 引入 第三方组件 的 JavaScript -->  
<script src="https://unpkg.com/element-plus/lib/index.full.js"></script>  
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>  


<script>
    // 请求cdns的资源
    async function get_cdns_data() {
        console.log('发送请求')
        const res = await axios.post("/api/source/get_study/cdn", "cdn")
        console.log(res)
        console.log(res.data.data)
        return res.data.data
    }
    // 创建列表来展示
    function show_cdns_data() {
        console.log("创建表格...")
        // 调用函数
        var datas = get_cdns_data()
        datas.then(data => {
            // 循环创建表格
            var tbody = document.getElementById("content")
            // keys
            var keys = ["name", "imp_url", "css_url"]
            for (var i = 0; i < data.length; i++) {
                console.log(data[i])
                // 循环创建tr
                var trItem = document.createElement('tr')
                // 创建th
                for (var j in data[i]) {  
                    var thItem = document.createElement('th')
                    thItem.textContent = data[i][j]
                    trItem.appendChild(thItem)
                }
                tbody.appendChild(trItem)
        }
        })

    }
    show_cdns_data()
</script>