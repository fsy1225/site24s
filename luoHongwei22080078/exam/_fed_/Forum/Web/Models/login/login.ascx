﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="login.ascx.cs" Inherits="Web.Models.login.login" %>

<style>
	.f{
		text-align: center;
		font-family: 华文楷体;
	}
</style>

<form class="f">
	<fieldset>
		<legend>登录/注册</legend>
		用户名<br /><input type="text" id="u" /><br />
		密码<br /><input type="password" id="p" /><br />
		<button>登录</button><br />
		<span style="color:red; font-size:8px">未注册的将在点击登录时自动注册!</span>
	</fieldset>
	
</form>

<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
	var but = document.querySelector('button')
	but.addEventListener('click', async (e) => {
		// 阻止默认提交
		e.preventDefault()
		// 发送post请求
		const res = await axios.post('/api/login', { username: u.value, password: p.value })
		// console.log(res)
		console.log(res.data)
		// 如果登录/注册成功
		if (res.data.bol === true) {
			console.log('跳转')
			window.location.href = "../index/index.aspx?username=" + u.value
		}


	})
</script>