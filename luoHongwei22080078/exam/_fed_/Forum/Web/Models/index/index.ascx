﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="index.ascx.cs" Inherits="Web.Models.index.index" %>


<!-- 其他控件或内容 -->  
  
<!-- 引入 Element Plus 的 CSS -->  
<link rel="stylesheet" href="https://unpkg.com/element-plus/lib/theme-chalk/index.css" runat="server" />  
  
<!-- 用户控件的特定内容 -->  
<style>
    .common-layout{
        margin:0px;
        padding:0px;
        font-family: 华文楷体;
    }
    .header{
        display:block;
        background-color: gray;
        font-family: 华文楷体;
        margin: 0px;
        padding:10px;
        text-align:center;
        font-size: 20px;
    }
    .header #user{
        font-size: 15px;
        position: absolute;
        top: 6px;
        right:8px;
        color:red;
    }
    .show_study_source{
        display:block;
        font-size:10px;
        width:auto;
        text-align:left;
        margin:0px;
        padding:1px;
        width:250px;
    }
    el-main{
        display:flex;
    }
    .show_study_source li{
        list-style-type:none;
        display:inline-block;
    }
    .article{
        display:inline-block;
        list-style-type:none;
    }
</style>



<div class="common-layout">
    <el-container>
        <el-header class="header">
            <span><b>欢迎来到零空论坛!</b></span>
            <a href="./user/user.aspx" id="user"></a>
        </el-header>
        <el-main>
            <div class="show_study_source" id="stu_src">
                <span style="font-size:20px">资源列表</span>
                <!--嵌入列表-->
                <ul id="cdns">
                    <li>
                       1.<a href="../coms/cdn/cdn.aspx">第三方组件库链接(cdn链接)</a>
                    </li>
                </ul>
            </div>
            <div class="article">
                <ul id="articles">
                </ul>
            </div>
        </el-main>
    </el-container>
</div>

<!-- 引入 第三方组件 的 JavaScript -->  
<script src="https://unpkg.com/element-plus/lib/index.full.js"></script>  
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>  

<script>  
    // 从cookie中获取用户名
    var value = document.cookie;
    var username = value.split("=").slice(-1).shift();
    var user = document.getElementById('user');
    user.innerHTML = username;
    console.log(username);
    // 请求article资源
    async function get_articles_data() {
        console.log("请求帖子资源")
        var res = await axios.post("/api/source/get_article", "article")
        console.log(res.data.data)
        return res.data.data
    }
    function show_ten_article() {
        console.log("创建帖子标题...")
        // 调用函数
        var datas = get_articles_data()
        datas.then(data => {
            // 循环创建列表
            var ul = document.getElementById("articles")
            for (var i = 0; i < data.length; i++) {
                console.log(data[i])
                // 循环创建li
                var liItem = document.createElement('li')
                // 循环创建a标签
                var a = document.createElement('a')
                a.href = '../coms/article/articleDetail.aspx'
                a.textContent = data[i]["name"]
                ul.appendChild(liItem)
            }
            
        })
    }
    show_ten_article()

</script>  
  
<!-- 其他控件或内容 -->