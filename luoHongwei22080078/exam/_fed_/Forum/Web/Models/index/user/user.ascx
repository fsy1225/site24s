﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="user.ascx.cs" Inherits="Web.Models.index.user.user" %>

<style>
    .show_info{
        display:block;
        text-align: center;
        position: absolute;
        top:50px;
        left: 300px;
        border: 1px solid;
        box-shadow: 1px 1px;
        background-color: lightskyblue;
        width: 200px;
    }
</style>

<link rel="stylesheet" href="https://unpkg.com/element-plus/lib/theme-chalk/index.css" runat="server" />  

<div class="show_info">
    <div class="header">
        <span>用户基本信息</span>
    </div>
    <hr />
    <div class="content">
        用户名(账号):<p id="username"></p>
        <hr />
        <button onclick="cancel()">退出登录</button>
    </div>


</div>

<script src="https://unpkg.com/element-plus/lib/index.full.js"></script>  
<script>
    var value = document.cookie;
    var username = value.split("=").slice(-1).shift();
    var user = document.getElementById("username")
    user.innerHTML = username
    function cancel() {
        if (confirm("确定要退出登录吗?")) {
            window.location.href="../../login/login.aspx"
        }
    }


</script>