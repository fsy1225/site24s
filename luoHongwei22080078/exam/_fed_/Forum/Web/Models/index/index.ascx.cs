﻿using System;
using System.Web;
using System.Web.UI;

namespace Web.Models.index
{
    public partial class index : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string username = this.Page.Request.QueryString["username"];
            HttpCookie login_cookie = new HttpCookie("login_cookie");
            login_cookie.Value = username;
            login_cookie.Path = "/";
            login_cookie.Expires = DateTime.Now.AddHours(2);
            Page.Response.Cookies.Add(login_cookie);
        }

    }
}