﻿CREATE TABLE [Stu_login].[usr]
(
	[id] INT NOT NULL PRIMARY KEY identity,
	[username] varchar(50),
	[password] varchar(50),
	_time datetime default getUtcDate(),
	_memo nvarchar(max),
)
