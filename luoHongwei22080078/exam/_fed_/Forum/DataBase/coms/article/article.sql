﻿CREATE TABLE [coms].[article]
(
	[Id] INT NOT NULL PRIMARY KEY identity,
	article_id INT NOT NULL UNIQUE,
	tp nvarchar(50),
	title nvarchar(50),
	content nvarchar(50),
)
