﻿CREATE TABLE [coms].[cdn]
(
	[Id] INT NOT NULL PRIMARY KEY identity,
	name nvarchar(100),
	imp_url nvarchar(max),
	css_url nvarchar(max),
)
