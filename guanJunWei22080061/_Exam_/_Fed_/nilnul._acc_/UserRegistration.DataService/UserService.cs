﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserRegistration.DataService
{
    class UserService
    {
    }
}

public class UserService
{
    private readonly UserRepository _userRepository;

    public UserService(UserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public void RegisterUser(string username,string password,string email)
    {
        var password = HashPassword(password);

        var user = new User
        {
            Username = username,
            Password = password,
            Email = email
        };

        _userRepository.AddUser(user);
    }
    private string HashPassword(string password)
    {

    }
}