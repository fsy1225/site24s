﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="UserRegistration.Web.Register" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>用户注册</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            用户名:<asp:TextBox ID="txtUsername" runat="server" />
            <br />
            密码:<asp:TextBox ID="txtPassword" runat="server" TextMode="Password" />
            <br />
            电子邮件:<asp:TextBox ID="txtEmail" runat="server" />
            <br />
            <asp:Button ID="btnRegister" runat="server" Text="注册"
OnClick="btnRegister_Click" />
        </div>
    </form>
</body>
</html>
