﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserRegistration.Api
{
    public class UsersContraoller
    {
    }
}

[ApiController]
[Route("api/[controller]")]
public class UsersController : ControllerBase
{
    private readonly UsersController _userService;

    public UsersController(UserService userService)
    {
        _userService = userService;
    }

    [HttpPost("register")]
    public IActionResult Register([FromBody] RegisterUserRequest request)
    {
        _userService.RegisterUser(request.Username, request.Password, request.Email);
        return Ok();
    }

}

public class RegisterUserRequest
{
    public string Username { get; set; }
    public string Password { get; set; }
    public string Email { get; set; }
}