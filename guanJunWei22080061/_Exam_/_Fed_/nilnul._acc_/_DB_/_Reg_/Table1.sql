﻿CREATE TABLE [dbo].[Table1]
(
    [UserID] int NOT NULL PRIMARY KEY identity(1,1), 
    [Username] NVARCHAR(50) NOT NULL, 
    [Password] NVARCHAR(256) NOT NULL, 
    [Email] NVARCHAR(100) NOT NULL, 
    [CreatedAt] DATETIME DEFAULT GETDATE() NULL
)
