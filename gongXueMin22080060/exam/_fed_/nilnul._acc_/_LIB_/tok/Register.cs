﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.acc.token  
{  
    public class UserRegistration  
    {  
      
        static public bool RegisterUser(string name, string pass)  
        {  
           string Password = EncryptPassword(pass); 
  
            using (var db = new nilnul._acc_._LINQ_.tok.DataClasses1DataContext())  
            {  
                try  
                {   
                    db.RegisterUser(name, Password); 
  
                    return true;  
                }  
                catch (Exception ex)  
                {   
                    Console.WriteLine("Registration failed: " + ex.Message);  
                    return false;  
                }  
            }  
        }  
    
        private static string EncryptPassword(string password)  
        {    
            return password; //   
        }  
    }  
}
