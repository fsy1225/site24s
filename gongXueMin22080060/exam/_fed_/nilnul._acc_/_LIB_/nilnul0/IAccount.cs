﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul
{
    /// <summary>
    /// marker interface 
    /// </summary>
    interface IAccount
    {
    }

    /// <summary>
    /// membered interface
    /// </summary>
    interface AccountI:IAccount {
        /// <summary>
        /// username
        /// </summary>
        string name { get;  }
    }
}
