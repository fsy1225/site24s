﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="nilnul._acc_._WEB_.Register" %>  
  
<!DOCTYPE html>  
  
<html xmlns="http://www.w3.org/1999/xhtml">  
<head runat="server">  
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>  
    <title>用户注册</title>  
</head>  
<body>  
    <form id="form1" runat="server">  
        <div>  
            <h2>用户注册</h2>  
            <asp:Label ID="Label1" runat="server" Text="用户名:"></asp:Label>  
            <asp:TextBox ID="txtUsername" runat="server"></asp:TextBox>  
            <br />  
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUsername" ErrorMessage="用户名不能为空！" Display="Dynamic">*</asp:RequiredFieldValidator>  
            <br />  
  
            <asp:Label ID="Label2" runat="server" Text="密码:"></asp:Label>  
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>  
            <br />  
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPassword" ErrorMessage="密码不能为空！" Display="Dynamic">*</asp:RequiredFieldValidator>  
            <br />  
  
            <asp:Label ID="Label3" runat="server" Text="确认密码:"></asp:Label>  
            <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>  
            <br />  
            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtConfirmPassword" ErrorMessage="密码不匹配！" Display="Dynamic">*</asp:CompareValidator>  
            <br />  
  
            <asp:Label ID="Label4" runat="server" Text="电子邮件:"></asp:Label>  
            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>  
            <br />  
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" ErrorMessage="请输入有效的电子邮件地址！" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic">*</asp:RegularExpressionValidator>  
            <br />  
  
            <asp:Button ID="btnRegister" runat="server" Text="注册" OnClick="btnRegister_Click" />  
        </div>  
    </form>  
</body>  
</html>