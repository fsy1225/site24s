﻿using System;
using System.Collections.Generic;
using System.Web.Http;

namespace WebApplication1.Controllers
{
    public class Data
    {
        public string Username { get; set; } // 使用 PascalCase 命名规范  
        public string Password { get; set; } // 使用 PascalCase 命名规范  
    }

    public class LoginController : ApiController // 控制器名称使用 PascalCase  
    {
        // GET api/login  
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/login/5  
        public string Get(int id)
        {
            return "value";
        }

        // POST api/register  
        [HttpPost]
        [Route("api/register")]
        public IHttpActionResult Register([FromBody] Data data) // 使用 'data' 代替 'value' 以增加可读性  
        { 
            return Ok();
        }

        // POST api/login  
        [HttpPost]
        [Route("api/login")]
        public IHttpActionResult Login([FromBody] Data data) 
        {
            var result = Lib.Login.Login(data.Username, data.Password); // 假设 Lib.Login.Login 返回一个结果对象  
            return Ok(result); 
        }

    
    }
}