﻿CREATE proc [nilnul._acc.usr.tok].[Logon]
(
	@name nvarchar(50),
	@pass nvarchar(50),
	@due datetime null out
	,
	@token uniqueidentifier =null out
	,
	@acc bigint = null out
)
AS
BEGIN
set nocount on
--SET FMTONLY OFF

set @acc =(
	select top 1 id from [nilnul].Acc
		where UPPER( name)=upper(@name) and [password]=@pass
);

if @acc is null begin
	return 1;
end

declare @tokens table([token] uniqueidentifier);

if @due is null begin
	insert [nilnul._acc.usr].[Token](
		[acc] 
	)
		output inserted.[token] into @tokens
		values(
			@acc
		)
	;
end
else begin
	insert [nilnul._acc.usr].[Token](
		[acc], due 
	)
		output inserted.[token] into @tokens
		values(
			@acc,
		
			@due
		)
	;
end

set @token= (select top 1 [token] from @tokens);

END
