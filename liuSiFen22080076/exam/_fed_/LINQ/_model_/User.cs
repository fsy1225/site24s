﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _model_
{
    public class User
    {
        private int Id;

        public int Id1 { get => Id; set => Id = value; }
        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public string Name { get => name; set => name = value; }
        public string Mobile { get => mobile; set => mobile = value; }
        public int State { get => state; set => state = value; }
        public DateTime Adddate { get => adddate; set => adddate = value; }

        private string username;
        private string password;
        private string name;
        private string mobile;
        private int state;
        private DateTime adddate;

    }
}
